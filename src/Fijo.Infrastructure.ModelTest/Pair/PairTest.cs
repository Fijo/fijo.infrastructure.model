using NUnit.Framework;

namespace Fijo.Infrastructure.ModelTest.Pair
{
    [TestFixture(typeof(Model.Pair.Pair<,>))]
    public class PairTest
    {
        [Test]
        public void GetHashCodeTest() {
        	var pair = new Model.Pair.Pair<object, object>(null, "test");
// ReSharper disable ReturnValueOfPureMethodIsNotUsed
			Assert.DoesNotThrow(() => pair.GetHashCode());
// ReSharper restore ReturnValueOfPureMethodIsNotUsed
        }
    }
}