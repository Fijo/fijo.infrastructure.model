﻿using System;
using System.Text;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Model {
	[Serializable]
	public struct KeyValuePair {
		public object Key { get; [UsedImplicitly] set; }
		public object Value { get; [UsedImplicitly] set; }

		public KeyValuePair(object key, object value) : this() {
			Key = key;
			Value = value;
		}

		public override string ToString() {
			var sb = new StringBuilder();
			sb.Append('[');
			if (Key != null) sb.Append(Key);
			sb.Append(", ");
			if (Value != null) sb.Append(Value);
			sb.Append(']');
			return sb.ToString();
		}
	}
}