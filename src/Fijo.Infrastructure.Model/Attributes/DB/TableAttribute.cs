﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fijo.Infrastructure.Model.DB.Attributes
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class TableAttribute : Attribute
    {
        public readonly string TableName;
        public readonly string DatabaseKey;

        public TableAttribute(string tableName, string databaseKey = "")
        {
            TableName = tableName;
            DatabaseKey = databaseKey;
        }
    }
}
