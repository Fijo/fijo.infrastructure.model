﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fijo.Infrastructure.Model.DB.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = false)]
    public class FieldAttribute : Attribute
    {
        public string Name;

        public FieldAttribute(string name)
        {
            Name = name;
        }
    }
}
