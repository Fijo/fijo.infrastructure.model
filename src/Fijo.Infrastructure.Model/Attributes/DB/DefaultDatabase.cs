﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fijo.Infrastructure.Model.DB.Attributes
{
    [AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
    public class DefaultDatabase : Attribute
    {
        public readonly string DatabaseKey;

        public DefaultDatabase(string databaseKey = "")
        {
            DatabaseKey = databaseKey;
        }
    }
}
