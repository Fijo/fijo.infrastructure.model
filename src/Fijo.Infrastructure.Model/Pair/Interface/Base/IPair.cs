using JetBrains.Annotations;

namespace Fijo.Infrastructure.Model.Pair.Interface.Base {
	[PublicAPI]
	public interface IPair<T1, T2> : IPair {
		[PublicAPI] void Create(T1 first, T2 second);
		[PublicAPI] T1 First { get; }
		[PublicAPI] T2 Second { get; }
	}

	[PublicAPI]
	public interface IPair {
		[PublicAPI] void Create(object first, object second);
		[PublicAPI] object GenericFirst { get; }
		[PublicAPI] object GenericSecond { get; }
	}
}