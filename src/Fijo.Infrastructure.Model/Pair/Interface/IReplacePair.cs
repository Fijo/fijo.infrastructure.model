using Fijo.Infrastructure.Model.Pair.Interface.Base;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Model.Pair.Interface {
	[PublicAPI]
	public interface IReplacePair<TEntry> : IReplacePair<TEntry, TEntry> {}

	[PublicAPI]
	public interface IReplacePair<TNeedle, TReplacement> : IPair<TNeedle, TReplacement> {
		[PublicAPI] TNeedle Needle { get; }
		[PublicAPI] TReplacement Replacement { get; }
	}
}