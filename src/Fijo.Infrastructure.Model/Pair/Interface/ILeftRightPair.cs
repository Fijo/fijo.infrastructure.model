using Fijo.Infrastructure.Model.Pair.Interface.Base;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Model.Pair.Interface {
	[PublicAPI]
	public interface ILeftRightPair<TObject> : IPair<TObject, TObject> {
		[PublicAPI] TObject Left { get; }
		[PublicAPI] TObject Right { get; }
	}
}