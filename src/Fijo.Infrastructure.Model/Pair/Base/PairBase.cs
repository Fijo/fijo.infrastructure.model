using System;
using System.Diagnostics;
using Fijo.Infrastructure.Model.Pair.Interface.Base;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Model.Pair.Base {
	[Serializable]
	public abstract class PairBase<TObject> : PairBase<TObject, TObject> {}
	
	[Serializable]
	public abstract class PairBase<TFirst, TSecond> : PairBase, IPair<TFirst, TSecond> {
		#region InternalFields
		private TFirst _internalFirst;
		private TSecond _internalSecond;

		[PublicAPI]
		protected TFirst InternalFirst {
			get {
				#region PreCondition
				EnsureCreated();
				#endregion
				return _internalFirst;
			}
			set { _internalFirst = value; }
		}
		[PublicAPI]
		protected TSecond InternalSecond {
			get {
				#region PreCondition
				EnsureCreated();
				#endregion
				return _internalSecond;
			}
			set { _internalSecond = value; }
		}
		#endregion

		#region Implementation of IPair<TFirst,TSecond>
		public void Create(TFirst first, TSecond second) {
			#region PreCondition
			CheckCreated();
			#endregion
			InternalCreate(first, second);
		}

		public virtual TFirst First { get { return InternalFirst; } protected set { InternalFirst = value; } }
		public virtual TSecond Second { get { return InternalSecond; } protected set { InternalSecond= value; } }
		#endregion
		
		protected virtual void InternalCreate(TFirst first, TSecond second) {
			InternalFirst = first;
			InternalSecond = second;
		}

		#region Implementation of IPair
		protected override void InternalCreate(object first, object second) {
			InternalCreate((TFirst) first, (TSecond) second);
		}

		public override object GenericFirst { get { return First; } }
		public override object GenericSecond { get { return Second; } }
		#endregion
	}

	[Serializable]
	public abstract class PairBase : IPair {
		#if MonitorPairCreation
		private bool _isCreated;
		#endif

		#region Implementation of IPair
		public void Create(object first, object second) {
			#region PreCondition
			CheckCreated();
			#endregion
			InternalCreate(first, second);
		}

		public abstract object GenericFirst { get; }
		public abstract object GenericSecond { get; }
		#endregion

		#region Create
		protected abstract void InternalCreate(object first, object second);
		
		[Conditional("MonitorPairCreation")]
		protected internal void CheckCreated() {
			#if MonitorPairCreation
			if (_isCreated) ThrowAlreadyCreated();
			_isCreated = true;
			#endif
		}
		
		[Conditional("MonitorPairCreation")]
		protected internal void EnsureCreated() {
			#if MonitorPairCreation
			if(!_isCreated) ThrowNotCreated();
			#endif
		}
		
		[Conditional("MonitorPairCreation"), TerminatesProgram]
		private void ThrowAlreadyCreated() {
			#if MonitorPairCreation
			throw new InvalidOperationException("The Pair is already created and can only be created once. If you use the constructor with some (> 0) arguments your pair will be created within the constructor.");
			#endif
		}

		[Conditional("MonitorPairCreation"), TerminatesProgram]
		private void ThrowNotCreated() {
			#if MonitorPairCreation
			throw new InvalidOperationException("The Pair you tried to use had not been created yet. Ensure that your pairs are already created by using it with a constructor with some arguments or by calling the Create- methode, after constructing and before any usage.");
			#endif
		}
		#endregion
		
		#region Equals
		protected bool Equals([NotNull] IPair other) {
			#region PreCondition
			EnsureCreated();
			#endregion
			return Equals(GenericFirst, other.GenericFirst) && Equals(GenericSecond, other.GenericSecond);
		}

		public override int GetHashCode() {
			#region PreCondition
			EnsureCreated();
			#endregion
			unchecked {
				return (NullableGetHashCode(GenericFirst) * 397) ^ NullableGetHashCode(GenericSecond);
			}
		}

		[Pure]
		private int NullableGetHashCode<T>(T me) {
			return !typeof(T).IsValueType && Equals(null, me) ? 0 : me.GetHashCode();
		}
		#endregion
	}
}