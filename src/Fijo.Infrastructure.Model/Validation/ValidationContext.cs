namespace Fijo.Infrastructure.Model.Validation {
	public class ValidationContext<TSource, TValidatior> {
		public readonly TSource Source;
		public readonly TValidatior Validatior;

		public ValidationContext(TSource source, TValidatior validatior) {
			Source = source;
			Validatior = validatior;
		}
	}
}