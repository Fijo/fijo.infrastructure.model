namespace Fijo.Infrastructure.Model.Validation {
	public class ValidationResult<TSource, TValidatior, TRule> : ValidationResult {
		public readonly ValidationContext<TSource, TValidatior> Context;
		public readonly TRule Rule;

		public ValidationResult(ValidationContext<TSource, TValidatior> context, TRule rule, bool result) : base(result) {
			Context = context;
			Rule = rule;
		}
	}

	public class ValidationResult {
		public readonly bool Result;
		public ValidationResult(bool result) {
			Result = result;
		}
	}
}